import java.util.Scanner;
public class LuckyCardGameApp{
	
	public static void main (String[] args){
		Deck myDeck = new Deck();
		
		myDeck.shuffle();
		
		Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to the Lucky Card Game!");
		System.out.println("Initial number of cards: " + myDeck.length());
		
		System.out.println("how many cards do you want to remove");
		int cardsToRemove = scanner.nextInt();		//stores desired num in variable
		
		
		for(int i = 0; i < cardsToRemove && myDeck.length() > 0; i++){
			myDeck.drawTopCard();
		}
		
		System.out.println("Number of cards after your removal: " + myDeck.length());
	
		myDeck.shuffle();
		System.out.println("Here are the cards after shuffling: " + myDeck.toString());
	}
}
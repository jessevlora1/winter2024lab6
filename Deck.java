import java.util.Random;
public class Deck{
	
	private Card[] cards;						//Fields
	private int numberOfCards;
	private Random rng;
	
	public Deck(){								//Constructor
		this.rng = new Random();
		this.cards = new Card[52];
		
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};		
        
		String[] values = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
		
		int cardIndex = 0;
		for(String suit : suits){
			for(String value : values){
				this.cards[cardIndex] = new Card(suit, value);
				cardIndex++;
			}
		}
		
		this.numberOfCards = 52;
	}
	
	public int length(){
        return this.numberOfCards;
    }
	
	public Card drawTopCard(){
		if(numberOfCards > 0){
			Card topCard = this.cards[numberOfCards - 1];
			
			numberOfCards--;
			
			return topCard;
		}
		else{
			return null;
		}
	}
	
	public String toString(){
		if(numberOfCards == 0){
			return " ";
		}
		
		String result = cards[0].toString();
		
		for(int i = 1; i < numberOfCards; i++){
			result += ", " + cards[i].toString();
		}
		
		return result;
	}
	
	public void shuffle(){
		for(int i = 0; i < numberOfCards; i++){			
			int randomIndex = rng.nextInt(numberOfCards); // this picks a number in the length of number of cards
			
			Card shuffledCard = this.cards[i];		//this card will get shuffled 
			this.cards[i] = this.cards[randomIndex];		//that card will now be card at 	newIndex
			
			this.cards[randomIndex] = shuffledCard;	//that card is now equal to the card we wanted to shuffle
		}
	}
}


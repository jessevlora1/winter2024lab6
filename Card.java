public class Card{
	
	private String suit;							//Fields
	private String value;
	
	public Card(String suit, String value){			//Constructor
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){						//getter
		return this.suit;
	}
	
	public String getValue(){						//getter
		return this.value;
	}
	
	public String toString(){						//TtoString
		return this.suit + " " + "of" + " "+ this.value;
	}
}
	